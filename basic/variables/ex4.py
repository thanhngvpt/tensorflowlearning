import tensorflow as tf
import numpy as np

x = tf.Variable(0, name='x')
model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)
    for i in range(5):
        x = x + np.random.randint(1000) - x
        print(session.run(x))
