import tensorflow as tf
import matplotlib.image as mpimg

# load image
filename = 'images/MarshOrchid.jpg'
image = mpimg.imread(filename)

# create TensorFlow variable
x = tf.Variable(image, name='x')

model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)
    width = tf.shape(x).get_shape()
    print('width of x: ', width[0])
