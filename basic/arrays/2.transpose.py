import tensorflow as tf
# import matplotlib
# matplotlib.use('Qt5Agg')

import matplotlib.image as mpimg
import matplotlib.pyplot as plt


def show_image(img):
    plt.imshow(img)
    plt.show()

# load the image
filename = "images/MarshOrchid.jpg"
image = mpimg.imread(filename)

# print out its shape
print(image.shape)

# # view image
# show_image(image)

# geometric manipulation
# create a TensorFlow variable
x = tf.Variable(image, name='x')

model = tf.global_variables_initializer()

with tf.Session() as session:
    x = tf.transpose(x, perm=[1, 0, 2])
    session.run(model)
    result = session.run(x)

# view image after turning 90 degree
show_image(result)
