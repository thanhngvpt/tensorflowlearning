import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import tensorflow as tf

# load image
filename = "images/MarshOrchid.jpg"
image = mpimg.imread(filename)
height, width, depth = image.shape

# create TensorFlow variable
x = tf.Variable(image, name='x')

model = tf.global_variables_initializer()

with tf.Session() as session:
    x = tf.transpose(x, perm=[1, 0, 2])
    x = tf.reverse_sequence(x, [height] * width, 1, batch_dim=0)
    session.run(model)
    result = session.run(x)

plt.imshow(result)
plt.show()
