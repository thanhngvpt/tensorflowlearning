import matplotlib.image as mpimg
import matplotlib.pyplot as plt

# load image
filename = "images/MarshOrchid.jpg"
image = mpimg.imread(filename)

# print its shape
print(image.shape)

# show image
plt.imshow(image)
plt.show()